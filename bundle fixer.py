"""
This changes the bundle metadata back to the way it used to be, so the Bundle Modder will access the files just fine.
Run this script before the Bundle Modder to fix the bundles. Whenever the game gets an update, this script must run again,
or the Bundle Modder will not work correctly.

The script reads in the all_h.bundle and uses the info to create individual all_xy_h.bundle.
As you can see below, I just feed in lots and lots of constants that I am not sure about.
The Bundle Modder and the game run fine though.

Requires Python: https://www.python.org/ftp/python/2.7.13/python-2.7.13.msi

The script can find the assets folder on its own on Windows.
Adjust and uncomment the path p below ONLY if you get issues. There should be no need to change anything on Windows.
Double click the file to run; or right click -> Edit with IDLE -> F5.
"""
from __future__ import print_function


##p = r"C:\Program Files (x86)\Steam\steamapps\common\PAYDAY 2\assets"

##################################
##################################
##################################
from binascii import hexlify, unhexlify
from collections import OrderedDict
import os, sys, shutil
from struct import pack, unpack

def AllMeta(source):
    """Read all_h.bundle."""
    f = open(source,"rb")
    eof, bundleCount, _,_,_ = unpack("5I",f.read(20))
    offsets, entryCounts, indexes = [], [], []
    for i in range(bundleCount):
        index, entryCount, entryCount2, offset, one = unpack("QIIQI",f.read(28))
        assert entryCount == entryCount2
        assert one == 1
        entryCounts.append(entryCount)
        offsets.append(offset+4)
        indexes.append(index)
        
    rv = []
    for i in range(bundleCount):
        dat = OrderedDict()
        f.seek(offsets[i])
        for entry in range(entryCounts[i]):
            index, offset, size = unpack("III",f.read(12))
            dat[index] = (offset, size)
        rv.append((indexes[i],dat))
    f.close()
    return rv
# If user did not bother to give the right path, try to figure it out.
if "p" not in globals():
    try:
        from _winreg import *
    except ImportError:
        from winreg import *
    paths = [QueryValueEx(OpenKey(HKEY_CURRENT_USER, r"SOFTWARE\Valve\Steam"), "SteamPath")[0]]
    if paths[0] and os.path.exists(paths[0]+"/steamapps/libraryfolders.vdf"):
        for line in open(paths[0]+"/steamapps/libraryfolders.vdf"):
            pieces = line.split('"')
            if len(pieces)==5 and pieces[3][1:4]==":\\\\":
                paths.append(pieces[3].replace("\\\\","/"))
    for p2 in [p2+"/steamapps/common/PAYDAY 2/assets" for p2 in paths]:
        if os.path.exists(p2):
            p=p2
            print("Automatically retrieved the path:",p)
            break
    else:
        print("Could not retrieve the path from the registry. You must manually change the path in the script.")
        ERROR
else:
    # This part exists only for advanced users: non-Win users or those with several installations.
    # Don't bother with too many sanity checks here.
    if not os.path.exists(p):
        print("Path p is specified but points to invalid directory.")
        ERROR
    print("Using the given path:",p)

# Move the all_h.bundle from the assets folder to the script folder.
##target = (os.path.dirname(__file__)+"/all_h.bundle").replace("\\","/").lower()
source = (p+"/all_h.bundle").replace("\\","/").lower()
##if os.path.exists(source) and source!=target:
##    if os.path.exists(target):
##        os.remove(target)
##    shutil.move(source, target)
##    print("Moved all_h.bundle from assets folder to",os.path.dirname(__file__))
##else:
##    if os.path.exists(target):
##        print("Using the local all_h.bundle.")
##    else:
##        print("Could not find all_h.bundle at all. Abort.")
##        ERROR

# Write the new _h bundles.
for i, dat in AllMeta(source):
    f = open(p+"/all_"+str(i)+"_h.bundle","wb")
    fileSize = 7*4 + len(dat)*12+4
    f.write(pack("I",fileSize))
    f.write(unhexlify("F297A000"))
    f.write(pack("II",len(dat),len(dat)))
    f.write(unhexlify("18000000ACEC180001ED1800"))
    for index, (offset, size) in dat.items():
        f.write(pack("III", index, offset, size))
    f.write(unhexlify("191FC59400000000"))
    f.close()
print("Success! Created the individual bundles necessary for Bundle Modder.")
